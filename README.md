# Intervention durant le séminaire Interactions Multimodales Par ECran (IMPEC)

## Proposition initiale
L'impact du livre numérique sur les pratiques d'édition : reconfiguration des processus de publication

Au-delà du succès mitigé du livre numérique, ce dernier impacte la façon de fabriquer des publications ou des livres, qu'ils soient imprimés ou _dématérialisés_.
L'analyse de publications numériques non conventionnelles permet de prendre la mesure du changement de paradigme en cours, il ne s'agit plus seulement de diffuser largement via les réseaux internet, mais d'intégrer de nouvelles dimensions aux documents dans leur consultation et leur fabrication.
De nouveaux objets éditoriaux exploitent de façon beaucoup plus essentielle les possibilités offertes par le numérique : interopérabilité permettant une circulation de l'information entre les acteurs et les machines ; modularité des outils de conception et de production ouvrant la perspective d'une horizontalité des interventions ; versions et formats divers s'adaptant aux pratiques multiples des lecteurs.
Nous nous concentrons ici sur l'impact que peuvent avoir ces processus sur les pratiques de lecture, de dissémination et d'écriture dans le champ scientifique.
En nous écartant des questions économiques souvent étudiées, nous souhaitons comprendre et analyser ce que fait le numérique à l'édition, en creux.

Ce parcours comprend un appui sur l'histoire de l'écriture et du livre, un regard en direction de la philosophie de la technique, des analyses de publications numériques originales ainsi que des perspectives pour l'édition _de demain_.


## Esquisse de plan

### Introduction

- contextualisation : non fiction, édition scientifique (pas forcément universitaire) ; le livre comme point de départ mais regard vers les publications au sens large (articles aussi)
- informatisation des métiers du livre bien avant le livre numérique : des bibliothèques aux métiers de l'édition
- le livre numérique apporte une nouvelle approche commune au web
- quelques éléments de définition : livre, édition, publication, livre numérique
- cette communication s'appuie en grande partie sur un mémoire et d'autres communications

### De nouvelles publications numériques
Point de départ : découverte de nouveaux objets numériques dans le domaine de la publication.

- présentation des livres numériques de Getty Publications
- présentation de Distill.pub
- ce que l'on peut en retenir : prise en compte des besoins divers des lecteurs, modification profonde des pratiques d'édition (même si à la marge)

kirschenbaum_track_2016, ludovico_post-digital_2016, vandendorpe_du_1999

### 3 principes
À partir de l'analyse de ces _nouvelles_ publications, définition de trois grands principes sur les pratiques d'édition (pratiques au sens de production). Appui sur le mémoire.

1. interopérabilité
2. modularité
3. multiformité

### Pistes et limites

- finalement le point de départ est ce que l'on peut attendre d'un document numérique : les 5 propriétés d'un document numérique
- difficultés de mise en pratique : de nouvelles dépendances
- enjeu de la reconfiguration du travail : crawford_eloge_2016
- quelques liens vers d'autres articles : Markdown, Git,

## Bibliographie
https://www.zotero.org/antoinentl/items/collectionKey/ABQ8X3I8
